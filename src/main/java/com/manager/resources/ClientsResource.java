package com.manager.resources;

import com.manager.models.Client;
import com.manager.persistence.ClientDao;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
@Component
public class ClientsResource {

    private ClientDao clientDao;
    @Inject
    public ClientsResource(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    /**
     * Get all clients
     * @return clients
     */
    @GET
    public List<Client> listClients() {
        List<Client> clients = this.clientDao.findAll();
        return clients;
    }

    /**
     * Add a new Client
     * @param client
     * @return new client
     */
    @POST
    public Client addClient(@Valid Client client) {
        return clientDao.save(client);

    }

    /**
     * Remove a client
     * @param nif
     */
    @DELETE
    @Path("{nif}/client")
    public void removeClient(@PathParam("nif") int nif) {
        Client client = clientDao.findOne(nif);
        if (client == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        } else {
            clientDao.delete(client);
        }
    }

    /**
     * Get an single Client
     * @param nif
     * @return client
     */
    @GET
    @Path("{nif}/client")
    public Client getClient(@PathParam("nif") int nif) {
        Client client = clientDao.findOne(nif);
        if (client == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        } else {
            return client;
        }
    }

    /**
     * Get the clients with the same name
     * @param name
     * @return clients
     */
    @GET
    @Path("name={name}")
    public List<Client> getClients(@PathParam("name") String name) {
        List<Client> result = new ArrayList<Client>();
        List<Client> clients = this.clientDao.findAll();
        for(Client c : clients) {
            if(c.getName().equals(name)) {
                result.add(c);
            }
        }
        return result;
    }
}
