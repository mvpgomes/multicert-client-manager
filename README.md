# Multicert Challenge : Client Manager

This repository contains the implementation of a REST application that allows to perform client management.

## Requirements

In order to run this project in your local machine, you need to have installed the following tools:

* Java 7
* Maven 3
* PostgreSQL 9

## Resources

The client manager allows to execute the following operations:

* List all Clients.
* Add a new Client.
* Remove an existent Client.
* Get an existent Client given his NIF.
* List all Clients with the same name.

In the next sections we will present a more detailed information on how to perform these operations.

To perform the requests to the client manager application we recommend that you use a Browser REST Client such as [Postman](http://www.getpostman.com/).

### List all Clients

To list the Clients that currently are registered in the system you need to perform a `GET` request for the `/clients` path.


### Add a new Client

To add a new client in the system you need to perform a `POST` request for the `/clients` path. This request requires that you send the parameters to create a new client, i.e, **NIF**, **Name**, **Address** and **Phone**.

To send parameters with the request you can create a JSON object as show in the example and then include in you request body:

```json
{
  "nif": 123456,
  "name": "John Doe",
  "address": "5th Avenue, New York, NY",
  "phone":  "+555 555 555"
}
```
### Delete a Client

To delete an existent Client from the system you need to perform a `DELETE` request to the `/clients` path and pass the Client NIF as a parameter in your request path `/{client_nif}/client`


### Get a Client

To get an existent Client from the system you need to perform a `GET` request to the `/clients` path and pass the Client NIF as a parameter in your request path `/{client_nif}/client`

### List all Clients with the same name

To list the Clients that have the same name you need to perform a `GET` request  pass the name as a parameter in your request path `/clients?name="client_name"`.

## Running the application

You can run the application in your local machine or if we want you can interact
with our REST application that is deployed on [Heroku](#heroku).

To run the application in your local machine, first you need to have a local instance of a PostgreSQL database.

### Starting the PostgreSQL server
Open you command-line and execute the following command:

Unix:

``
$  postgres -D /usr/local/pgsql/data &
``

OSX:

``
$ postgres -D /usr/local/var/postgres &
``

This will start the PostgreSQL server in background.

### Creating a database
We need to create a database named `multicert-demo`. To do that execute the following command:

``
$ createdb multicert-demo
``

 Now you need to access the new database. To do that you need to execute the following command:

``
$ psql multicert-demo
``

### Creating a database table
Now we need to create a table for our Clients that will stored in the database. To do that you need to execute the following command:

``
$ create table client (nif serial NOT NULL, name varchar(255) NOT NULL, address varchar(255) NOT NULL, phone varchar(255) NOT NULL, CONSTRAINT pk_client_nif PRIMARY KEY (nif));
``

This will create a table to store our users.

### Setting Up the database for our application
Finally, we need to create an user and grant some permissions for our application in the new database.

#### Creating an User
We need to create the user `multicert` and set its password for the database. To do that execute the following command:

``
$ CREATE USER multicert WITH PASSWORD 'multicert';
``

#### Granting Permissions
We need to grant access permissions for our new user. To do that execute the following commands:

``
$ GRANT ALL PRIVILEGES ON TABLE client to multicert;
``    

and

``
$ GRANT USAGE, SELECT ON SEQUENCE client_nif_seq TO multicert;
``

### Starting the REST application
To deploy the application in your local machine, first we need to set up some environment variables.
Go to the project root directory and execute the following commands:

``
$ export JDBC_DATABASE_URL="jdbc:postgresql://127.0.0.1:5432/multicert-demo"
``

``
$ export JDBC_DATABASE_USERNAME="multicert"
``

``
$ export JDBC_DATABASE_PASSWORD="multicert"
``

Now you are ready to deploy the application. To do that execute the following command:

``
$ mvn clean package && mvn spring-boot:run
``

After the application deployment is finished your application will running in `http://localhost:8080`.

### Heroku
Currently we have our REST application deployed on Heroku at `https://multicert-demo.herokuapp.com/`.
